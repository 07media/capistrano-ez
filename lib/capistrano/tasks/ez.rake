namespace :ez do
    namespace :db do
        task :setup do
            invoke 'ez:db:create'
            invoke 'ez:'
        end

        desc 'Creates a databse on the `:db` role.'
        task :create do
            on roles(:db) do
                execute :mysql, '-e "CREATE DATABASE ez_%s DEFAULT CHARACTER SET utf8 COLLATE utf8_danish_ci;"' % fetch(:application)
            end
        end

        desc <<-DESC
            Crates a database user, and assigns permissions to hostnames defined in `:db_grant_list`

            This will generate a password and a MySQL user, and grant permissions
            for the user to the site's database.
        DESC
        task :create_user do
            # Generate a random 10 character password, and remove the new line.
            password = `head -c 10 /dev/urandom | base64`.strip

            on roles(:db) do
                info ("MySQL password for ez_%s is '%s'" % [fetch(:application), password])
                exit 0
                # Create the user
                execute :mysql, %{-e "CREATE USER '%s'@'localhost' IDENTIFIED BY '%s'"} % [fetch(:application), password]

                # For each host, grant permission for the user on that specific
                # host to the site's database.
                fetch(:db_grant_list).each do |host|
                    execute :mysql, %{-e "GRANT ALL PRIVILEGES ON ez_%s.* TO '%s'@'%s' IDENTIFIED BY '%s';"} % [
                        fetch(:application), fetch(:application), host, password
                    ]
                end
            end
        end
    end

    namespace :legacy do
        # Appends the legacy log directory for the site to the linked
        # direcotries array. We need to do this in a task, rather than
        # `defaults.rb`, as we don't have access to `:application` there because
        # the site's `deploy.rb` is read after `default.rb`.
        task :append_application_log_linked_dirs do
            set :linked_dirs, (fetch(:linked_dirs) || []).push("ezpublish_legacy/var/#{fetch(:application)}/log")
        end

        # We must run this on this on the production server, so the
        # index_{rest,cluster}.php files are created with the correct paths.
        task :assets_install do
            invoke 'symfony:command', 'ezpublish:legacy:assets_install', '--no-interaction --symlink --relative'
        end
    end

    task :build do
        run_locally do
            legacy_dir = "#{fetch(:build_dir)}/#{fetch(:legacy_path)}"

            within fetch(:build_dir) do
                if test "[ -d ezpublish/config/prod ]"
                    execute :cp, "ezpublish/config/prod/* ezpublish/config/"
                end

                execute :composer, :install, fetch(:composer_install_flags)

                if test "[ -f package.json ]"
                    execute :npm, :rebuild
                end

                if test "[ -f Gruntfile.js ]"
                    execute :grunt, :build
                end
            end

            within legacy_dir do
                # Set up the environment's cluster settings
                cluster_config = "config.cluster.#{fetch(:stage)}.php"
                if test "[ -f #{legacy_dir}/#{cluster_config} ]"
                    execute :mv, '-f', cluster_config, 'config.cluster.php'
                end

                # Handle environment specific ini-settings.
                # Find all ini settings, and comment out everything between
                # the matching env, and the end of the file.
                execute :find, '. -regex ".+\.ini.+?" | xargs sed -i "/^\#env=%s/,$ s/^/#/"' % fetch(:legacy_ini_disable_level)

                execute :php, 'bin/php/ezpgenerateautoloads.php'
            end
        end
    end

    task :set_permissions do
        on release_roles :all do
            execute :chown, '-R %s:%s' % [fetch(:web_user), fetch(:web_group)], current_path
            execute :chown, '-R %s:%s' % [fetch(:web_user), fetch(:web_group)], release_path
        end
    end

    task :create_storage do
        on release_roles :all do
            execute :mkdir, '-pv /srv/%s/shared' % fetch(:application)
            execute :chown, '-R %s:%s /srv/%s/shared' % [fetch(:web_user), fetch(:web_group), fetch(:application)]
        end
    end

    after 'rsync:stage', 'ez:build'
    after 'deploy:updating', 'ez:legacy:assets_install'
    before 'deploy:check:linked_dirs', 'ez:legacy:append_application_log_linked_dirs'
end

namespace :deploy do
    task :finishing do
        invoke 'ez:set_permissions'
    end

    namespace :check do
        task :directories do
            #marked out by SvM 2014111
	    #invoke 'ez:create_storage'

            on release_roles :all do
                execute :chown, '-R %s:%s' % [fetch(:web_user), fetch(:web_group)], deploy_path
            end
        end
    end

    # By default, capistrano/symfony runs composer with the `--no-scripts` flag.
    # Running it without said flag will build the bootstrap. I'm unsure why
    # it would run with this flag (contradicting capistrano/composer's default
    # flags). I believe running composer with post-install scripts is required
    # for eZ to run to some degree.
    #
    # This will simply clear the `build_bootstrap` task, so we avoid running it
    # twice.
    #
    # See also: https://github.com/capistrano/symfony/pull/1
    if Rake::Task.task_defined?(:build_bootstrap)
        Rake::Task['build_bootstrap'].clear_actions
    end

    # Ensure we do not run `composer:install` on the live server as a part of
    # the normal deploy process.
    Rake::Task['updated'].prerequisites.delete 'composer:install'
end
