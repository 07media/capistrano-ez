# Overrides capistrano-symfony's defaults.
# See: https://github.com/capistrano/symfony/blob/master/lib/capistrano/symfony/defaults.rb

# Symfony application path
set :app_path, 'ezpublish'

# Ez legacy path
set :legacy_path, 'ezpublish_legacy'

# Which level of ini-setting should be enabled.
# Syntax is:
#   #env=:legacy_ini_disable_level
# Everything below will be commented out.
set :legacy_ini_disable_level, fetch(:stage) == :production ? 'pre-production' : 'local'

# Path to Symfony's console
set :symfony_console_path, fetch(:app_path) + '/console'

# Path to Symfony's log files
set :log_path, fetch(:app_path) + '/logs'

# Path to Symonfy's cache files
set :cache_path, fetch(:app_path) + '/cache'

# Path to Symony's config files
set :app_config_path, fetch(:app_path) + '/config'

# Directories which are shared across releases
# See also `ez:legacy:append_application_log_linked_dirs` task.
set :linked_dirs, [
    fetch(:log_path),
    'ezpublish_legacy/var/log'
]

# Location of assets
set :assets_install_path, fetch(:web_path)

# Controllers to delete before deploying
set :controllers_to_clear, ['index_dev.php']

# Composer install flags
# capistrano-symfony runs with `--no-scripts` by default, which is removed here.
# See: https://github.com/capistrano/symfony/pull/1
set :composer_install_flags, "--no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction"

# List of servers/hostnames which have access to the mysql server
set :db_grant_list, [
    '192.168.254.%', # San servers
    'imbrium.muspel.07.no'
]