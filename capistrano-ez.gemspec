# -*- encoding: utf-8 -*-

Gem::Specification.new do |gem|
    gem.name = 'capistrano-ez'
    gem.version = '0.1'
    gem.authors = ['Torjus Bjåen']
    gem.email = ['torjus@07.no']
    gem.summary = 'Ez specific Capistrano tasks'
    gem.description = <<-DESC
        Provides a handful of tasks and defaults for simple deployment of
        eZ 5.x sites.
    DESC

    gem.files = `git ls-files`.split($/)
    gem.require_paths = ['lib']

    gem.add_dependency 'capistrano', '~> 3.2'
    gem.add_dependency 'capistrano-rsync', '~> 1.0.2'

    # Not on Rubygems.org. Use:
    #   gem 'capistrano-symfony', '~> 0.1', :github => 'capistrano/symfony'
    # in Gemfile.
    #
    # See also: https://github.com/capistrano/symfony/pull/5
    gem.add_dependency 'capistrano-symfony', '~> 0.1'
end
